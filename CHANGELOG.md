Changelog
### 1.0.1 (2023-02-24)


### 🚚 Chores

* **changelog:** 🔧 add changelog configuration ([eb0d74c](https://gitlab.com/grupojaque/oeio/oeio-app-android/commit/eb0d74c393e6ae8d9dd019de17bb0f8172d6d717))
